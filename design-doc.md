# 1. Camera Reviews
---
- [1. Camera Reviews](#1-camera-reviews)
  - [1.1. Overview:](#11-overview)
    - [1.1.1. Scope](#111-scope)
      - [1.1.1.1. Supported use-cases](#1111-supported-use-cases)
      - [1.1.1.2. Out of Scope](#1112-out-of-scope)
- [2. Proposal](#2-proposal)
  - [2.1. Architecture](#21-architecture)
    - [2.1.1. Architecture diagram](#211-architecture-diagram)
        - [2.1.1.0.1. External components](#21101-external-components)
    - [2.1.2. Diagram](#212-diagram)
    - [2.1.3. Actors](#213-actors)
    - [2.1.4. UseCase Diagram](#214-usecase-diagram)
      - [2.1.4.1. **Admin**](#2141-admin)
      - [2.1.4.2. **Editor**](#2142-editor)
      - [2.1.4.3. **Reader**](#2143-reader)
    - [2.1.5. Sequence Diagram](#215-sequence-diagram)
      - [2.1.5.1. **Create new Editor**](#2151-create-new-editor)
    - [2.1.6. Models](#216-models)
      - [2.1.6.1. Entities](#2161-entities)
      - [2.1.6.2. Entity Relationship Diagram](#2162-entity-relationship-diagram)
      - [2.1.6.3. Json](#2163-json)
  - [2.2. Limitations](#22-limitations)
  - [2.3. Costs](#23-costs)


## 1.1. Overview:
This project is a platform for photogarphers mainly located in Central America and South America. They create reviews of cameras and lenses with intentions of adding more products to review in a future. The public for this reviews are going to be around the globe, so it is expected millions of readers.

- Editors can upload reviews of cameras
- Editors can upload reviews of lenses
- Read the content in a website with mobile and desktop versions.
- Adminstration of editors excluding visitors to the website.

### 1.1.1. Scope
#### 1.1.1.1. Supported use-cases
* Readers can acces to all reviews without any credentials
* The actions of readers are *read-only*
* Editors and readers can access to the website from their mobiles or desktop
* Only editors can upload reviews of products*
* Editors have the permisions to update their own reviews, not from others
* Deleteing reviews is not allowed
* The system supports high quality upload of images made with the cameras
* Only editors need credentials to upload reviews

**products meaning that we might add more products to review aside form cameras and lenses.*
#### 1.1.1.2. Out of Scope
* Profiles of visitors
* Deleting of reviews
* Schedule of review releases

# 2. Proposal
---
## 2.1. Architecture

### 2.1.1. Architecture diagram
The following diagram show the approach of microservices usings two different tecnologies in the backend side (**.net** and **nodejs**). The client side will use **ReactJs**, in the middle a gateway as a bridge in between. The **.net** microservice will take the task of uploading reviews and handling autentication for *editors*. The **nodejs** microservice will be read-only to retrive reviews for *readers* with low latency.

The NoSQL database will store data from *editor*, reviews, role and role and permissions data. The SQL database will sync with the NoSQL thorough a service, so will have consistency and availavility wherever a *reader* needs it.

##### 2.1.1.0.1. External components
Cloudinary third-party service will host the images uploaded by the *editors*

### 2.1.2. Diagram

![Architecure diagram](./assets/Infrastructure.drawio.svg)


### 2.1.3. Actors
The actors represent the roles they can aquiare while interacting with the system

![Roles](/assets/Infrastructure-Page-2.drawio.svg)

- **Admin**: Has control over the system, resource management *(editors and reviews)* **requires credentials*
- **Editor**: Role of the reviewer than can create new resources such as reviews, upload images. **requires credentials*
- **Reader**: Do not require any credentials to retrieve reviews

### 2.1.4. UseCase Diagram

![Resource management](./assets/Infrastructure-Page-3.drawio.svg)
#### 2.1.4.1. **Admin**
|      UseCase      |          Description           |
| :---------------: | :----------------------------: |
| Create new editor | Create new user editor profile |
| Terminate editor  |   Desactivate editor account   |
|   Edit Reviews    |  Modify or soft delete review  |
|   Read Reviews    |          Read reviews          |

#### 2.1.4.2. **Editor**
|    UseCase     |                Description                |
| :------------: | :---------------------------------------: |
| Create Reviews |             Create new review             |
| Update Reviews | Update reviews created by the same editor |
|  Read Reviews  |               Read reviews                |

#### 2.1.4.3. **Reader**
|    UseCase     |                Description                |
| :------------: | :---------------------------------------: |
|  Read Reviews  |               Read reviews              |


### 2.1.5. Sequence Diagram

#### 2.1.5.1. **Create new Editor**
```mermaid
sequenceDiagram
    actor Admin
    Admin->>+Login: Use credentials
    Login-->>-Admin: Session/JWT
    Admin->>+Panel: Redirect
    Note right of Panel: Click Add Editor
    Panel->>+Form: Add info of new editor
    Form->>-Panel: success
```
** add other cases
### 2.1.6. Models

#### 2.1.6.1. Entities

```mermaid
erDiagram  
    ROLE {
        int Id
        string Name
    }
    USER {
        int Id
        string FirstName
        string LastName
        string Email
        date Birthday
        string Password
        int RoleId
    }
    REVIEW {
        int Id
        string Title
        string Content
        string ImageUrl
        date CreatedAt
        date UpdaredAt
        int EditorId
        int ProductId
    }
    CATEGORY {
        int Id
        string Name
    }
    PRODUCT {
        int Id
        string Name
        string Brand
        string Description
        decimal Price
        int CategoryId
    }
```

#### 2.1.6.2. Entity Relationship Diagram

```mermaid
erDiagram  
    CART ||--|{ CARTITEM : contains
    CART {
        int Id
        int UserId
    }
    PRODUCT ||--|| CARTITEM : contains
    PRODUCT {
        int Id
        string Name
        string Description
        string ImageURL
        decimal Price
        int Qty
        string CategoryId
    }
    CARTITEM {
        int Id
        int CartId
        int ProductId
        int Qty
    }
    USER ||--|| CART : contains
    USER {
        int Id
        string UserName
    }
    PRODUCTCATEGORY ||--|| PRODUCT : contains
    PRODUCTCATEGORY{
        int Id
        string Name
    }
```

#### 2.1.6.3. Json

Json responses


---

## 2.2. Limitations
Lista de limitaciones conocidas. Puede ser en formato de lista.
Ej.
* Llamadas a la API para obtener listas de productos, no excedelos limites de latencia 100ms
* No se soporta mas de X llamadas por segundo

*they need to be quantifiable*
*if the system grows then it could fail, so it is important to have all these details written*

---

## 2.3. Costs
Description and cost analysis
Contemplando 1000 usuarios diarios, que visitan recurrentemente cada hora:
Example:
"Considerando N usuarios diarios, M llamadas a X servicio/baseDatos/etc"
* 1000 llamadas diarias a serverless functions. $XX.XX
* 1000 read/write units diarias a X Database on-demand. $XX.XX
Total: $xx.xx (al mes/dia/año)
